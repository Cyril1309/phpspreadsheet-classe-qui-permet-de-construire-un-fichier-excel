<?php

#Use PhpOffice with composer.
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class generateExcel{

    protected $title;
    protected $description;
    protected $creator;
    protected $lastModified;
    protected $subject;
    protected $keywords;
    protected $category;

    /**
     * @param $title
     * @param $description
     * @param $creator
     * @param $lastModified
     * @param $subject
     * @param $keywords
     * @param $category
     */
    public function __construct(string $title, string $description, string $creator = "The Creator", string $lastModified = "The Modified", string $subject = "The Subject", string $keywords = "The Keywords", string $category = "The Category")
    {
        $this->title = $title;
        $this->description = $description;
        $this->creator = $creator;
        $this->lastModified = $lastModified;
        $this->subject = $subject;
        $this->keywords = $keywords;
        $this->category = $category;
    }

    /**
     * Permet de créer le fichier excel.
     * @return Spreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function create(){
        $objPHPExcel = new Spreadsheet();
        $objPHPExcel->getProperties()->setCreator($this->creator);
        $objPHPExcel->getProperties()->setLastModifiedBy($this->lastModified);
        $objPHPExcel->getProperties()->setTitle($this->title);
        $objPHPExcel->getProperties()->setSubject($this->subject);
        $objPHPExcel->getProperties()->setDescription($this->description);
        $objPHPExcel->getProperties()->setKeywords($this->keywords);
        $objPHPExcel->getProperties()->setCategory($this->category);

        $objPHPExcel->removeSheetByIndex(
            $objPHPExcel->getIndex(
                $objPHPExcel->getSheetByName('Worksheet')
            )
        );

        return $objPHPExcel;
    }

    /**
     * Permet de convertir un nombre en lettre de l'alphabet.
     * @param $number
     * @return string
     */
    public function getLettersByNumber(int $number): string{
        $letter = '';
        $num_letters = 26;
        $division = intval(($number - 1) / $num_letters);
        $reste = ($number - 1) % $num_letters;

        if ($division > 0) {
            $letter .= self::getLettersByNumber($division);
        }

        $letter .= chr(ord('a') + $reste);

        return $letter;
    }

    /**
     * Permet d'ajouter une feuille dans le fichier excel, vous pouvez personnaliser la feuille en jouant avec les paramètres.
     * @param $name
     * @param $header
     * @param $data
     * @param $objPHPExcel
     * @param $colorHeaders
     * @param $autosize
     * @param $formatCell
     * @return mixed
     */
    public function createSheet(string $name, array $header, array $data, $objPHPExcel, string $colorHeaders = null, bool $autosize = true, $formatCell = \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING){
        $totalWorksheet = $objPHPExcel->getSheetCount();
        $currentWorksheet = $totalWorksheet;

        $addWorksheet = new \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet($objPHPExcel, $name);
        $objPHPExcel->addSheet($addWorksheet, $currentWorksheet);

        # HEADER
        $countHeaders = count($header);
        if($colorHeaders == null){
            $objPHPExcel->getSheet($currentWorksheet)->fromArray($header, '', 'A1');
        }else{
            $objPHPExcel->getSheet($currentWorksheet)->fromArray($header, '', 'A1')->getStyle('A1:'.strtoupper($this->getLettersByNumber($countHeaders)).'1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB($colorHeaders);
        }

        # DATA
        if($autosize == true){
            foreach(range('A',strtoupper($this->getLettersByNumber($countHeaders))) as $columnID) {
                $objPHPExcel->getSheet($currentWorksheet)->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
        }

        $objPHPExcel->getSheet($currentWorksheet)->getStyle('A1:' . $objPHPExcel->getSheet($currentWorksheet)->getHighestColumn() . $objPHPExcel->getSheet($currentWorksheet)->getHighestRow())
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_TEXT);

        $objPHPExcel->getSheet($currentWorksheet)->fromArray($data, '', 'A2');

        # RETURN
        return $objPHPExcel;
    }

    /**
     * Permet en fonction de son paramétre uploader le fichier excel dans le dossier upload temp file ou alors le faire télécharger dans le navigateur.
     * @param $objPHPExcel
     * @param string $type
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function save($objPHPExcel, string $type = "tmp"){

        $objWriter = new Xlsx($objPHPExcel);
        switch ($type){
            case "tmp": #Upload temp file
                $pathTmpFile = "/chemin/vers/le/dossier";
                if (!is_dir($pathTmpFile)) {
                    mkdir($pathTmpFile, 0777, true);
                }
                $objWriter->save($pathTmpFile.$this->title.".xlsx");
                break;
            case "nav": #Navigateur
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename="'.$this->title.'.xlsx"');
                $objWriter->save('php://output');
                exit();
                break;
        }
    }
}