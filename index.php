<?php

require 'classes/generateExcel.class.php';

$headerUsers = array('id', 'name', 'pseudo', 'password');
$dataUsers = array(
        1 => array('1', 'joe', 'joe77', 'password'),
        2 => array('1', 'dwayne', 'dwayneNC', 'password'),
);

$excel = new generateExcel("Name for XLSX file", "Description for XLSX file");
$obj = $excel->create();
$obj = $excel->createSheet("users", $headerUsers, $dataUsers, $obj, '78D400'); #Ajouter autant de ligne que de fichier dans votre excel.
$obj = $excel->save($obj, "nav");